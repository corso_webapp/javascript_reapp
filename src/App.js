import React from 'react';
import {BrowserRouter as Router, Route, Link} from "react-router-dom"; 
import {getRecipes, deleteRecipe} from "./fakeAPI";

// importo i componenti
import AllRecipes from "./Components/AllRecipes";
import MainComponent from "./Components/MainComponent";
import RecipeDetail from "./Components/RecipeDetail";
import AddRecipeForm from "./Components/AddRecipeForm";
import AddIngredientForm from "./Components/AddIngredientForm";



// DA QUI COMINCIA LA NOSTRA APPLICAZIONE
function App() {
	return (
		<div>
			{/*L'elemento BrowserRouter è padre di tutti gli altri, gli passa delle props per gestire la navigazione tra le pagine*/}
			<Router>
				{/*Link finto, della libreria "react-router-dom". punta alla home. Messo qui in modo che appaia in tutte le pagine (una navbar andrebbe qui)*/}
				<Link to="/"> <h1>MEKKA RICETTARIO LASER!!</h1></Link>
				<div>
					{/*Ogni Route corrisponde ad un componente da caricare, facciamo finta che siano pagine diverse.
						l'operatore ":" ci permette di specificare un parametro nell'URL.*/}
					<Route exact path="/" component={MainComponent} />
					<Route exact path="/recipes" component={AllRecipes}  />
					<Route exact path="/recipes/:id" component={RecipeDetail}  />
					<Route exact path="/add-recipe" component={AddRecipeForm}  />
					<Route exact path="/add-ingredient" component={AddIngredientForm}  />
				</div>
			</Router>
		</div>

	);
}

export default App;
