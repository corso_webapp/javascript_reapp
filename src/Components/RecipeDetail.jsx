import React from 'react';
import {getIngredients, getRecipes} from '../fakeAPI';
import { Link } from 'react-router-dom';

// Component che ci mostra i dettagli di una ricetta
class RecipeDetail extends React.Component {
	// COSTRUTTORE, primo ad essere chiamato
	constructor(props) { 	//1
		super(props); 
		// inizializzo lo stato ma non faccio chiamate asincrone.
		// una buona norma potrebbe essere impostare uno stato che indichi il caricamento del component.
		this.state = {
			allRecipes: [],
			allIngredients: [],
			currentRecipe: undefined
		};

	}

	// LIFECYCLE, componentDidMount. chiamato SOLO UNA VOLTA, finchè il componente non viene smontato (nel nostro caso un cambio di pagina)
	// ATTENZIONE!! Non viene chiamato se cambia l'URL!!!
	// se passo da   /recipes/1   a   /recipes/3   non viene chiamata questa funzione!!!
	componentDidMount(){
		// qui posso fare le chiamate asincrone al database
		this.setState({
			allRecipes: getRecipes(),
			allIngredients: getIngredients()
		});
	}

	// LIFECYCLE, componentDIdUpdate. chiamato molte volte, quando lo state o le props vengono modificate
	// questo vuol dire:
	//		* quando si chiama this.setState({.......});
	//		* ad esempio quando cambia l'URL (perchè cambiano le props, il component non viene smontato)
	// prevProps e prevState contengono le props e lo state prima dell'update
	// quelli nuovi sono già nel this
	componentDidUpdate(prevProps, prevState){    //4
		// a questo punto il component è stato aggiornato, e il render chiamato con il nuovo stato/props
		// se non controllo che le cose siano cambiate e faccio un aggiornamento di stato si CICLA ALL'INFINITO!!!
		if (this.state.allRecipes !== prevState.allRecipes 
			|| this.state.allIngredients !== prevState.allIngredients
			|| this.props.match.params.id !== prevProps.match.params.id){
			this.setRecipe(parseInt(this.props.match.params.id));
		}
	}

	// seleziono la ricetta da mostrare
	setRecipe(recipeId) {
		for (let rIndex in this.state.allRecipes) {
			let recipe = this.state.allRecipes[rIndex];
			if (recipe.id === recipeId) {
				this.setState({currentRecipe: recipe});
			}
		}
	}

	// nome di un ingrediente dato il suo id
	getIngredientName(ingredientId){
		for (let ingrIndex in this.state.allIngredients){
			if(this.state.allIngredients[ingrIndex].id === ingredientId){
				return this.state.allIngredients[ingrIndex].name;
			}
		}
	}

	// LIFECYCLE, si chiama subito dopo il COSTRUTTORE, poi ogni volta che lo stato o le proprietà cambiano
	// viene chiamato prima anche di "componentDidUpdate"
	render() {		//2
		if (this.state.currentRecipe) {
			return (
			<div className="row justify-content-center">
				<div className="col-md-6 card p-1 m-1">
					<center>
						<div className="card-header">
							<h3>{this.state.currentRecipe.title}</h3>
						</div>
						<div className="card-body">
							<p>{this.state.currentRecipe.description}</p>
							<ul>
								{this.state.currentRecipe.ingredients
									.map((id) =>
										<div key={"i_"+id} className="list-group-item">
											{this.getIngredientName(id)}
										</div> 
										)}
							</ul>
						</div>
					</center>
				</div>
			</div>
			);
		} else {
			return (<h1>ah boh non lo so io</h1>)
		}
	}
}

export default RecipeDetail;