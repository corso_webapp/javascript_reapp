import React from 'react';
import {addIngredient} from '../fakeAPI';
import { Link, Redirect } from 'react-router-dom';

// Form per aggiungere una ricetta nel database
class AddIngredientForm extends React.Component{
	// COSTRUTTORE, primo ad essere chiamato
	constructor(props) {
		super(props); 
		this.state = { 
			redirect: false			
		}
	}

	// aggiungi una nuovo ingrediente nel database e fai un redirect
	addNewIngredient(){
		let name = document.getElementById("ingredient-name").value;
		if(name){
			addIngredient(name);
			// sarebbe da fare in modo asincrono
			this.setState({redirect:true});
		}
	}

	// LIFECYCLE, si chiama subito dopo il COSTRUTTORE, poi ogni volta che lo stato o le proprietà cambiano
	render(){
		// redirect ad una pagina, con elemento di react-router-dom
		if (this.state.redirect){
			return(<Redirect to="/"/>);
		}
		// return normale
		return(
			<div className="row justify-content-center">
				<div className = "col-md-6 card p-1 m-1">
					<center>
						<p>Nome:</p> 
						<input id = "ingredient-name"/>
						<button className="btn btn-block btn-outline-success" onClick={() => this.addNewIngredient()}>
							AGGIUNGI!
						</button>
					</center>
				</div>
			</div>
		);
	}


}

export default AddIngredientForm;