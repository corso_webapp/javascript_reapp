import React from 'react';
import { Link } from "react-router-dom";
import {getRecipes, deleteRecipe} from "../fakeAPI";

// Functional component che rappresenta la view di una ricetta
function RecipeView(props) {
	return (<div className="col-md-5 card p-1 m-1">
		<center>
			<div className="card-header">
				<h3>{props.title}</h3>
			</div>
			<div className="card-body">
				<Link to={"/recipes/"+props.id}>
					<button className="btn btn-block btn-outline-success">
						Dettagli
					</button>
				</Link>
				<hr />
				<button className="btn btn-block btn-outline-danger" 
					onClick={props.deleteCallback}>
					Delete
				</button>
			</div>
		</center>
	</div>)
}

function Li(props) {
  return <div className="list-group-item" key={props.key}>
    {props.children}
  </div>
}


//Component che mostra tutte le ricette
class AllRecipes extends React.Component {
	constructor(props) {		// <-- constructor è una keyword
								//		cosa farà mai?
		// la superclasse, ha delle props a sua volta
		// questo le passa a lei
		super(props); //SUPERPROPS!!!!!
		//fin'ora sarebbe esattamente un react component normale no?
		// ora di aggiungere altra roba!!
		this.state = {  // <-- il malvagio **this** di cui parlavamo l'atra volta
			recipes: getRecipes()				// <----|
		}										//		|	
	}											//		|
												//		|
	deleteFirstRecipe() {						//		|
		let id = this.state.recipes[0].id;		//		|
		deleteRecipe(id);						//		|
		this.setState({							//		|
			recipes: getRecipes()		// <- vi ricorda niente questo json??
		});
	}

	// elimina una ricetta tramite il suo id
	deleteRecipeById(id) {
		deleteRecipe(id);
		this.setState({
			recipes: getRecipes()
		})
	}

	// LIFECYCLE, si chiama subito dopo il COSTRUTTORE, poi ogni volta che lo stato o le proprietà cambiano
	render() {
		return(
			<div>
				{	
					// IMPORTANTE viene passata una funzione di callback! 
					// questo è l'unico modo in cui un compoente figlio può "intervenire" su di un padre.
					this.state.recipes.map((r) => <RecipeView key={r.id+"-kabobo"} title={r.title} id={r.id} deleteCallback={() => this.deleteRecipeById(r.id)}/>)
				}
			</div>
		)
	}

}

export default AllRecipes;