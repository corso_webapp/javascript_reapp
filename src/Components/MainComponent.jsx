import React from 'react';
import { Link } from "react-router-dom";
import {getRecipes, getIngredients} from "../fakeAPI";


// Questo component sarà il nostro randomizzatore
class MainComponent extends React.Component {
	// COSTRUTTORE, primo ad essere chiamato
	constructor(props) {
		super(props); 
		// inizializzo lo stato ma non faccio chiamate asincrone.
		// una buona norma potrebbe essere impostare uno stato che indichi il caricamento del component.
		this.state = { 
			allRecipes: [],
			allIngredients: [],
			selectedIngredients: [],
			selectedRecipe: undefined			
		}
	}

	// LIFECYCLE, componentDidMount. chiamato SOLO UNA VOLTA, finchè il componente non viene smontato (nel nostro caso un cambio di pagina)
	componentDidMount(){
		// qui posso fare le chiamate asincrone al database
		this.setState({
			allRecipes: getRecipes(),
			allIngredients: getIngredients()
		});
	}

	// nome di un ingrediente dato il suo id
	getIngredientName(ingredientId){
		for (let ingrIndex in this.state.allIngredients){
			if(this.state.allIngredients[ingrIndex].id === ingredientId){
				return this.state.allIngredients[ingrIndex].name;
			}
		}
	}

	// aggiungi un ingrediente alla lista di ingredienti selezionati nello stato del componente
	addIngredient(){
		let select = document.getElementById("ingredient-select");
		let id = parseInt(select.options[select.selectedIndex].value);
		
		let currentIngredients = this.state.selectedIngredients;
		let alreadySelected = false;
		for(let index in currentIngredients){
			if(currentIngredients[index] === id){
				alreadySelected = true;
				break;
			}
		}
		if(! alreadySelected){
			currentIngredients.push(id);
		}
		this.setState({selectedIngredients:currentIngredients});
	}

	// scegli una ricetta random tra quelle per cui si hanno tutti gli ingredienti
	randomize(){
		let availableRecipes = [];
		for (let recipeIndex in this.state.allRecipes){
			let recipe = this.state.allRecipes[recipeIndex];
			let addRecipeFlag = true;
			for(let ingrIndex in recipe.ingredients){
				let ingredientId = recipe.ingredients[ingrIndex];
				if(this.state.selectedIngredients.indexOf(ingredientId) === -1){
					addRecipeFlag = false;
					break;
				}
			}
			if(addRecipeFlag){
				availableRecipes.push(recipe);
			}
		}
		if(availableRecipes.length > 0){
			let choice = availableRecipes[Math.floor(Math.random()*availableRecipes.length)];
			this.setState({selectedRecipe : choice});
		}
		else{
			this.setState({selectedRecipe : undefined});
		}
	}

	// LIFECYCLE, si chiama subito dopo il COSTRUTTORE, poi ogni volta che lo stato o le proprietà cambiano
	render() {
		return(
			<div>
				<h3>Dicci quello che hai in frigo, noi ti diremo chi sei!</h3>
				<hr/>
				{this.state.selectedRecipe 
					? <div className="card"><div className="card-header"><h2>{this.state.selectedRecipe.title}</h2></div></div> 
					: null
				}
				<hr/>
				<span>
					<ul>
					{this.state.selectedIngredients.map((ingrId)=><li key={"sel_"+ingrId}>{this.getIngredientName(ingrId)}</li>)}
					</ul>
					<select className="custom-select" id="ingredient-select" onClick={()=> {console.log("onClick");
							this.addIngredient();
							}}>
						{this.state.allIngredients.map((ingr)=> <option key={"opt_"+ingr.id} value={ingr.id}>
						{ingr.name}
							</option>)}
					</select>
				</span>
				<button className="btn btn-block btn-outline-success" onClick={() => this.randomize()}>
					CERCA!
				</button>
				<p> Se vuoi scorrere tutte le ricette, clicca <Link to="/recipes">QUI</Link>.</p>
				<p> Se vuoi aggiungere una nuova ricetta, clicca <Link to="/add-recipe">QUI</Link>.</p>
				<p> Se vuoi aggiungere un nuovo ingrediente, clicca <Link to="/add-ingredient">QUI</Link>.</p>
			</div>
		)
	}

}

export default MainComponent;