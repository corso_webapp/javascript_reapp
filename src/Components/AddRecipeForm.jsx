import React from 'react';
import {getIngredients, getRecipes, addRecipe} from '../fakeAPI';
import { Link, Redirect } from 'react-router-dom';

// Form per aggiungere una ricetta nel database
class AddRecipeForm extends React.Component{
	// COSTRUTTORE, primo ad essere chiamato
	constructor(props) {
		super(props); 
		// inizializzo lo stato ma non faccio chiamate asincrone.
		// una buona norma potrebbe essere impostare uno stato che indichi il caricamento del component.
		this.state = { 
			allRecipes: [],
			allIngredients: [],
			selectedIngredients: [],
			redirect: false			
		}
	}

	// LIFECYCLE, componentDidMount. chiamato SOLO UNA VOLTA, finchè il componente non viene smontato (nel nostro caso un cambio di pagina)
	componentDidMount(){
		// qui posso fare le chiamate asincrone al database
		this.setState({
			allRecipes: getRecipes(),
			allIngredients: getIngredients()
		});
	}


	// nome di un ingrediente dato il suo id
	getIngredientName(ingredientId){
		for (let ingrIndex in this.state.allIngredients){
			if(this.state.allIngredients[ingrIndex].id === ingredientId){
				return this.state.allIngredients[ingrIndex].name;
			}
		}
	}

	// aggiungi un ingrediente alla lista di ingredienti selezionati nello stato del componente
	addIngredient(){
		let select = document.getElementById("ingredient-select");
		let id = parseInt(select.options[select.selectedIndex].value);
	
		let currentIngredients = this.state.selectedIngredients;
		let alreadySelected = false;
		for(let index in currentIngredients){
			if(currentIngredients[index] === id){
				alreadySelected = true;
				break;
			}
		}
		if(! alreadySelected){
			currentIngredients.push(id);
		}
		this.setState({selectedIngredients:currentIngredients});
	}

	// aggiungi una nuova ricetta nel database e fai un redirect
	addNewRecipe(){
		let title = document.getElementById("recipe-title").value;
		let descr = document.getElementById("recipe-descr").value;
		let ingre = this.state.selectedIngredients;
		if(title && descr && ingre.length > 0){
			addRecipe(title, descr, ingre);
			this.setState({redirect:true});
		}
	}

	// LIFECYCLE, si chiama subito dopo il COSTRUTTORE, poi ogni volta che lo stato o le proprietà cambiano
	render(){
		// redirect ad una pagina, con elemento di react-router-dom
		if (this.state.redirect){
			return(<Redirect to="/recipes"/>);
		}
		// return normale
		return(
			<div className="row justify-content-center">
				<div className = "col-md-6 card p-1 m-1">
					<center>
						<div className= "card-header">
							<p>Titolo:</p> 
							<input id = "recipe-title"/>
						</div>
						<div className= "card-body">
							<p>Descrizione:</p> 
							<textarea id = "recipe-descr"/>
							<div>
								<p>Ingredienti:</p> 
								<ul>
								{this.state.selectedIngredients.map((ingr_id)=><li key={"sel_"+ingr_id}>{this.getIngredientName(ingr_id)}</li>)}
								</ul>
								<select className="custom-select" id="ingredient-select" onClick={()=>this.addIngredient()}>
									{this.state.allIngredients.map((ingr)=> <option key={"opt_"+ingr.id} value={ingr.id} >{ingr.name}</option>)}
								</select>
								<button className="btn btn-block btn-outline-success" onClick={() => this.addNewRecipe()}>
									AGGIUNGI!
								</button>
							</div>
						</div>
					</center>
				</div>
			</div>
		);
	}


}

export default AddRecipeForm;